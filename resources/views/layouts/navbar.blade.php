<div>
	<ul>
		<li><a href="{{ secure_url('/') }}">Home</a></li>
	</ul>
	<ul>
		@foreach (App\Util::all() as $util)
			<li><a href="{{ route('util.show', ['util' => $util->getName()]) }}"><span class="navlink">{{ title_case($util->name) }}</span></a></li>
		@endforeach
	</ul>
</div>