<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ env('APP_NAME') }}</title>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @include('layouts.header')
            @yield('content')
            @include('layouts.footer')
        </div>
    </body>
</html>