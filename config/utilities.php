<?php

$utils = [
	0 => ['name' => 'monokai', 'fullName' => 'Monokai Color Scheme', ],
	1 => ['name' => 'wattcalc', 'fullName' => 'Microwave Wattage Converter', ],
	3 => ['name' => 'specs', 'fullName' => 'Server Specifications', ],
];

return $utils;