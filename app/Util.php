<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Util extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'utils';

    /**
     * Override for the name of the primary key
     *
     * @var string
     */
    protected $primaryKey = 'name';

    /**
     * Override the primary key default behaviour
     * 
     * @var boolean
     */
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name', 'fullName',
    ];

    public function getName() {
    	return $this->name;
    }

    public function getFullName() {
    	return $this->fullName;
    }

    public function getRoute() {
    	return $this->route;
    }
}
