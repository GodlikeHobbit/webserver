<?php

namespace App\Http\Controllers;

use App\Util;
use Illuminate\Http\Request;

class UtilController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Garden  $garden
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $util = Util::findOrFail($name);
        return View('util' . '.' . $name)->with("util", $util);
    }
}
