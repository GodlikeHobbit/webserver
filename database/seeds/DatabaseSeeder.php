<?php

use Illuminate\Database\Seeder;
use App\Util;

class DatabaseSeeder extends Seeder
{

    public function insertUtil($util = null)
    {   
        $UtilID = DB::table('utils')->insertGetId([
            'name' => $util['name'],
            'fullName' => $util['fullName'],
            'route' => 'utils' . '.' . $util['name']
        ]);
        return $UtilID;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach (Config::get('utilities') as $util) {
            DatabaseSeeder::insertUtil($util);
        }
    }
}
