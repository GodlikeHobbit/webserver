<?php

Route::get('/', function () {
    return view('home');
});

Route::get('util/{util}', ['as' => 'util.show', 'uses' => 'UtilController@show']);
